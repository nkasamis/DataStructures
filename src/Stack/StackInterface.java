package Stack;

public interface StackInterface<T> {
    public void push(T data) throws RuntimeException;

    public T pop() throws RuntimeException;

    public T peek() throws RuntimeException;

    public boolean isEmpty();

    public void empty();
}
