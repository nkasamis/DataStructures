package Stack;

public class ArrayStack<T> implements StackInterface<T> {
    private static final int DEFAULT_LENGTH = 10;
    private int top;
    private T[] Array;

    public ArrayStack(int length) {
        if(length <= 0) {
            Array = (T[]) new Object[DEFAULT_LENGTH];
        } else {
            Array = (T[]) new Object[length];
        }
        top = -1;
    }

    public ArrayStack() {
        this(DEFAULT_LENGTH);
    }

    public boolean isEmpty() {
        return top == -1;
    }

    public T peek() {
        if(isEmpty()) {
            throw new StackException("Stack is empty");
        }
        return Array[top];
    }

    public T pop() {
        T x = peek();
        Array[top] = null;
        top--;

        return x;
    }

    public void push(T data) {
        if(top == Array.length) {
            throw new StackException("Stack overflow");

        }
        top++;
        Array[top] = data;
    }

    public void empty() {
        for(int i = 0; i <= top; i++) {
            Array[i] = null;
        }
        top = -1;
    }

    public String toString() {
        if(isEmpty()) {
            return "[ ]";
        }
        StringBuffer result = new StringBuffer("[");
        for(int i = 0; i < top; i++) {
            result.append(Array[i] + "]");
        }
        result.append(Array[top] + "]");

        return result.toString();
    }

    public static void main(String[] args) {
        ArrayStack<Integer> stack = new ArrayStack<>(5);

        try {
            System.out.println(stack);

            for(int i = 0; i < 5; i++) {
                stack.push(i);
            }
            System.out.println(stack);

            for(int i = 0; i < 3; i++) {
                stack.pop();
            }
            System.out.println(stack);

        } catch (StackException err) {
            System.err.println(err);
        }
    }
}
