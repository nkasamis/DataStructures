package LinkedList;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<T> implements Iterable<T> {
    Element<T> head;
    int listLength;

    public DoublyLinkedList() {
        head = null;
        listLength = 0;
    }



    private static class Element<T> {
        private T data;
        private Element<T> prev;
        private Element<T> next;

        public Element(T data, Element<T> prev, Element<T> next) {
            this.data = data;
            this.prev = prev;
            this.next = next;
        }
    }

    public Iterator<T> iterator() {
        return new DoublyLinkedListIterator();
    }

    private class DoublyLinkedListIterator implements Iterator<T> {
        private Element<T> prevElement;
        private Element<T> nextElement;

        public DoublyLinkedListIterator() {
            nextElement = head;
            prevElement = nextElement.prev;
        }

        public boolean hasNext() {
            return nextElement != null;
        }

        public boolean hasPrev() {
            return prevElement != null;
        }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T res = nextElement.data;
            nextElement = nextElement.next;

            return res;
        }

        public T prev() {
            if (!hasPrev()) throw new NoSuchElementException();
            T res = prevElement.data;
            prevElement = prevElement.prev;

            return res;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

    }
}
