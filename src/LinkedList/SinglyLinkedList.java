package LinkedList;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SinglyLinkedList<T> implements Iterable<T> {
    private Element<T> head;
    private int listLength;

    public SinglyLinkedList() {
        head = null;
        listLength = 0;
    }

    // Check if list is empty
    public boolean isEmpty() {
        return head == null;
    }

    // Empty the list
    public void empty() {
        head = null;
    }

    // Insert value in the front of list
    public void insertFront(T value) {
        head = new Element<>(value, head);
        listLength += 1;
    }

    // Remove first element
    public void removeFront() {
        Element<T> secondElement = head.next;
        head.data = secondElement.data;
        head.next = secondElement.next;
        listLength -= 1;
    }

    // Get first element
    public T getFront() {
        if(isEmpty()) {
            throw new NoSuchElementException();
        }
        return head.data;
    }

    // Insert value in the back of list
    public void insertBack(T value) {
        if(isEmpty()) {
            insertFront(value);
            listLength += 1;
        } else {
            Element<T> lastElement = head;
            while(lastElement.next != null) {
                lastElement = lastElement.next;
            }
            lastElement.next = new Element<T>(value, null);
            listLength += 1;
        }
    }

    // Remove last element
    public void removeBack() {
        Element<T> secondLastElement = head;
        while(secondLastElement.next != null && secondLastElement.next.next != null) {
            secondLastElement = secondLastElement.next;

        }

        secondLastElement.next = null;
        listLength -= 1;
    }

    // Get last element
    public T getBack() {
        if(isEmpty()) {
            throw new NoSuchElementException();
        }
        Element<T> lastElement = head;
        while(lastElement.next != null) {
            lastElement = lastElement.next;
        }
        return lastElement.data;
    }

    // Insert an element after index pos
    public void insertAfter(int pos, T data) {
        if(isEmpty() || pos >= listLength || pos < 0) {
            throw new IndexOutOfBoundsException();
        }
        else {
            Element<T> elem = head;
            for(int i = 0; i < pos; i++) {
                elem = elem.next;
            }
            elem.next = new Element<T>(data, elem.next);
            listLength += 1;
        }
    }

    public void insertBefore(int pos, T data) {
        if(pos == 0) {
            insertFront(data);
        } else if(pos > 0 && pos < listLength) {
            insertAfter(pos-1, data);
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    // For printing string representation of list
    public String toString() {
        StringBuilder result = new StringBuilder();
        for(Object x : this) {
            result.append(x + ", ");
        }

        return result.toString();
    }

    // Private subclass for list elements
    private static class Element<T> {
        private T data;
        private Element<T> next;

        public Element(T data, Element<T> next) {
            this.data = data;
            this.next = next;
        }
    }

    public Iterator<T> iterator() {
        return new SinglyLinkedListIterator();
    }

    private class SinglyLinkedListIterator implements Iterator<T> {
        private Element<T> nextElement;

        public SinglyLinkedListIterator() {
            nextElement = head;
        }

        public boolean hasNext() {
            return nextElement != null;
        }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            T res = nextElement.data;
            nextElement = nextElement.next;

            return res;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

    public static void main(String[] args) {
        SinglyLinkedList<Character> sll = new SinglyLinkedList<>();
        sll.insertFront('y');
        sll.insertFront('e');
        sll.insertFront('H');

        System.out.println("List: " + sll);
        System.out.println("List length: " + sll.listLength);
        System.out.println("First element: " + sll.getFront());
        System.out.println("Last element: " + sll.getBack());

    }
}
